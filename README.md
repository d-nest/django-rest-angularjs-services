# Django REST AngularJS Services


# Overview

Django REST AngularJS Services add a way to build automatically your AngularJS Services from your API docs.


# Requirements

* [Django REST Framework (3.5.3)][django-rest-framework]
* [Django REST Swagger (0.3.10)][django-rest-swagger]

# Installation

Install using `pip`...

    pip install -e git+git@bitbucket.org:d-nest/django-rest-angularjs-services.git#egg=django-rest-angularjs-services

Add `'rest_angularjs_services'` to your `INSTALLED_APPS` setting.

    INSTALLED_APPS = (
        ...
        'rest_angularjs_services',
        ...
    )

    Note:    
    This project depends on django-rest-framework library, so install it if you haven’t done yet. Make sure also you have installed rest_framework app.

And, if you are using rest_framework_swagger, adding the following setting will add a custom button into the swagger template making it essay to download the AngularJS Services.
    
    SWAGGER_SETTINGS = {
        ...
        'template_path': 'rest_framework_swagger/custom.html',
        ...
    }

Add rest_rest_angularjs_services urls:

    urlpatterns = patterns('',
        ...
        url(r'^rest-builder/', include('rest_angularjs_services.urls'))
        ...
    )

[django-rest-framework]: http://www.django-rest-framework.org/
[django-rest-swagger]: https://github.com/marcgibbons/django-rest-swagger
