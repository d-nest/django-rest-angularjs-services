from __future__ import unicode_literals

from . import utils


def build_parameter_context(parameter):
    context = parameter
    context['name_in_camel_case'] = utils.snake_case_to_camel_case(parameter.get('name', ''))
    return context


def build_operation_context(method, operation):
    context = operation
    context['name_in_camel_case'] = utils.snake_case_to_camel_case(operation.get('operationId', ''))

    parameter_list = []
    for parameter in operation.get('parameters', []):
        if method == 'get':
            parameter_list.append(build_parameter_context(parameter))
        else:
            if 'schema' in parameter:
                for prop in parameter['schema']['properties']:
                    param = parameter['schema']['properties'][prop]
                    param['name'] = prop
                    parameter_list.append(build_parameter_context(param))
    context['parameters'] = parameter_list
    context['is_retrieve'] = operation.get('operationId', '').endswith('read')
    return context


def get_endpoint_operations(endpoint, operations, elements_dict):
    split_endpoint = utils.split_endpoint(endpoint)
    element = {
        'path': split_endpoint[-1].strip('/'),
    }
    if len(split_endpoint) > 1:
        element_path = split_endpoint[-2].strip('/')
        element['element_path'] = element_path
        element['name'] = utils.url_case_to_camel_case(element_path)[:-1]

    operation_list = []
    for method in operations:
        current_operation = build_operation_context(method, operations[method])
        current_operation.update({
            'path': endpoint.strip('/'),
            'split_path_last': split_endpoint[-1].strip('/'),
            'split_path': split_endpoint,
            'endpoint': endpoint,
            'method': method
        })
        operation_list.append(current_operation)

        if len(split_endpoint) > 1 and not current_operation['is_retrieve']:
            element_path = element['element_path']
            if element_path in elements_dict:
                elements_dict[element_path]['operations'] += [current_operation]
            else:
                elements_dict[element_path] = element
                elements_dict[element_path]['operations'] = [current_operation]

    return operation_list, elements_dict


def build_api_service_context(service_name, service_paths, common_path_len):
    """
    Request docs for specific api of the remote framework
    :param service_name
    :param service_paths
    :return:
    """
    api_context = {
        'name_in_came_case': utils.snake_case_to_camel_case(service_name),
        'operations': []
    }
    elements_dict = {}
    for path, operations in service_paths:
        operation_list, elements_dict = get_endpoint_operations(path[common_path_len - 1:], operations, elements_dict)
        api_context['operations'] += operation_list

    api_context['elements'] = elements_dict.values()
    return api_context
