"""
______ _____ _____ _____                            _            _____  _____          
| ___ \  ___/  ___|_   _|                          | |          |___  |/  ___|         
| |_/ / |__ \ `--.  | |     __ _ _ __   __ _ _   _ | | __ _ _ __    | |\ `--.          
|    /|  __| `--. \ | |    / _` | '_ ` / _` | | | || |/ _` | '__|   | | `--. \         
| |\ \| |___/\__/ / | |   | (_| | | | | (_| | |_| || | (_| | |  /\__/ //\__/ / _  _  _         
\_| \_\____/\____/  \_/    \__,_|_| |_|\__, |\___/ |_|\__,_|_|  \____/ \____/ (_)(_)(_)      
                                         _/ |
                                        |__/
"""


__title__ = 'Django REST AngularJS Services'
__version__ = '0.0.1'
__author__ = 'The Nest Agency'
__license__ = 'BSD 2-Clause'
__copyright__ = 'Copyright 2017 The Nest Agency'

# Version synonym
VERSION = __version__

# Header encoding (see RFC5987)
HTTP_HEADER_ENCODING = 'iso-8859-1'

# Default datetime input and output formats
ISO_8601 = 'iso-8601'
