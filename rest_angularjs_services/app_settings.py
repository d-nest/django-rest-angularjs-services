import os

from django.conf import settings

DEFAULT_SETTINGS = {
    'api_docs_root': os.path.join(settings.MEDIA_ROOT, 'api_docs'),
}

API_DOCS_ROOT = getattr(settings, 'API_DOCS_ROOT', DEFAULT_SETTINGS['api_docs_root'])
