from __future__ import unicode_literals

from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt

from . import utils
from . import zipping
from . import building
from . import app_settings as settings

import os
import time


def builder_post_view(request):
    time_str = time.strftime("%Y%m%d-%H%M%S")
    api_directory_name = "api_%s" % time_str
    api_directory_path = os.path.join(settings.API_DOCS_ROOT, api_directory_name)
    utils.mkdir(api_directory_path)

    base_api_path = "%s?format=openapi" % request.META['HTTP_REFERER'].rstrip('/')

    remote_data = utils.get_remote_data(base_api_path, request.COOKIES)

    paths = remote_data.get('paths', {})
    common_path = os.path.commonprefix(paths.keys()).rsplit('/', 1)[0] + '/' 
    common_path_len = len(common_path)

    services = set()
    services_paths = {}

    api_context = {'apis': []}
    for path in paths:
        service_name = path[common_path_len:].split('/', 1)
        service_name = "%s" % utils.url_case_to_camel_case(service_name[0])

        services.add(service_name)
        try:
            services_paths[service_name] += [(path, paths[path])]
        except:
            services_paths[service_name] = [(path, paths[path])]

    for service_name in services_paths:
        paths = services_paths[service_name]
        directory_path = os.path.join(api_directory_path, service_name)
        utils.mkdir(directory_path)

        context = building.build_api_service_context(service_name=service_name,
                                                     service_paths=paths,
                                                     common_path_len=common_path_len)

        utils.render_to_file(filename='%s.module.js' % service_name,
                             directory_path=directory_path,
                             template='rest_angularjs_service_builder/module.html',
                             context=context)
        utils.render_to_file(filename='%s.services.js' % service_name,
                             directory_path=directory_path,
                             template='rest_angularjs_service_builder/service.html',
                             context=context)
        utils.render_to_file(filename='%s.config.js' % service_name,
                             directory_path=directory_path,
                             template='rest_angularjs_service_builder/config.html',
                             context=context)

    api_context['apis'] = list(services)
    utils.render_to_file(filename='api.services.js',
                         directory_path=api_directory_path,
                         template='rest_angularjs_service_builder/api.module.html',
                         context=api_context)

    zip_filename = '%s.zip' % time_str
    zip_file_path = os.path.join(settings.API_DOCS_ROOT, zip_filename)
    zipping.zipdir(api_directory_path, zip_file_path)

    output_stream = open(zip_file_path, "rb")
    response = HttpResponse(output_stream.read(), content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename="' + zip_filename + '"'
    output_stream.close()
    return response


def builder_view(request):
    if request.method != 'GET':
        return HttpResponseBadRequest()
    else:
        return builder_post_view(request)


builder_view = csrf_exempt(builder_view)

