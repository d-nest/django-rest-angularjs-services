from __future__ import unicode_literals

"""This is a sample function for zipping an entire directory into a zipfile"""

# This seems to work OK creating zip files on both windows and linux. The output
# files seem to extract properly on windows (built-in Compressed Folders feature,
# WinZip, and 7-Zip) and linux. However, empty directories in a zip file appear
# to be a thorny issue. The solution below seems to work but the output of
# "zip_info" on linux is concerning. Also the directory permissions are not set
# correctly for empty directories in the zip archive. This appears to require
# some more in depth research.

# I got some info from:
# http://www.velocityreviews.com/forums/t318840-add-empty-directory-using-zipfile.html
# http://mail.python.org/pipermail/python-list/2006-January/535240.html
import os
import zipfile


def zipdir(dir_path=None, zip_file_path=None, include_dir_in_zip=True):
    """Create a zip archive from a directory.

    Note that this function is designed to put files in the zip archive with
    either no parent directory or just one parent directory, so it will trim any
    leading directories in the filesystem paths and not include them inside the
    zip archive paths. This is generally the case when you want to just take a
    directory and make it into a zip file that can be extracted in different
    locations.

    Keyword arguments:

    dir_path -- string path to the directory to archive. This is the only
    required argument. It can be absolute or relative, but only one or zero
    leading directories will be included in the zip archive.

    zip_file_path -- string path to the output zip file. This can be an absolute
    or relative path. If the zip file already exists, it will be updated. If
    not, it will be created. If you want to replace it from scratch, delete it
    prior to calling this function. (default is computed as dir_path + ".zip")

    include_dir_in_zip -- boolean indicating whether the top level directory should
    be included in the archive or omitted. (default True)

    """
    if not zip_file_path:
        zip_file_path = dir_path + ".zip"
    if not os.path.isdir(dir_path):
        raise OSError("dir_path argument must point to a directory. "
                      "'%s' does not." % dir_path)
    parent_dir, dir_to_zip = os.path.split(dir_path)

    # Little nested function to prepare the proper archive path

    def trim_path(path):
        archive_path = path.replace(parent_dir, "", 1)
        if parent_dir:
            archive_path = archive_path.replace(os.path.sep, "", 1)
        if not include_dir_in_zip:
            archive_path = archive_path.replace(dir_to_zip + os.path.sep, "", 1)
        return os.path.normcase(archive_path)

    out_file = zipfile.ZipFile(zip_file_path, "w",
                               compression=zipfile.ZIP_DEFLATED)

    for (archivedir_path, dir_names, file_names) in os.walk(dir_path):
        for fileName in file_names:
            file_path = os.path.join(archivedir_path, fileName)
            out_file.write(file_path, trim_path(file_path))
    out_file.close()
