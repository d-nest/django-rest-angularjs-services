from __future__ import unicode_literals

from django.template.loader import render_to_string

import os
import re
import json
import urllib


def render_to_file(directory_path, filename, template, context):
    filename = os.path.join(directory_path, filename)
    open(filename, "w").write(render_to_string(template, context))


def mkdir(directory_path):
    try:
        os.makedirs(directory_path)
    except:
        pass


def get_remote_data(url, cookies):
    opener = urllib.request.build_opener()
    opener.addheaders.append(('Cookie', 'sessionid=%s' % cookies.get('sessionid', '')))
    data = opener.open(url)
    json_data = json.loads(data.read().decode('utf-8'))
    return json_data


def snake_case_to_camel_case(snake_str):
    components = snake_str.lower().replace('-', '_').split('_')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return components[0] + "".join(x.title() for x in components[1:])


def slug_case_to_camel_case(url_str):
    components = url_str.strip('/').lower().split('-')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return components[0] + "".join(x.title() for x in components[1:])


def url_case_to_camel_case(url_str):
    components = url_str.strip('/').lower().replace('-', '/').split('/')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return slug_case_to_camel_case(components[0]) + "".join(slug_case_to_camel_case(x).title() for x in components[1:])


def split_endpoint(path):
    try:
        return re.split(r"/\{[a-zA-Z0-9_]+\}/", path[1:])
    except Exception as e:
        print(e.message)
        return []
