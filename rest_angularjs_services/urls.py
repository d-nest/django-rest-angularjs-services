from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^get/angularjs-components/$', views.builder_view, name='download_angularjs_components'),
]
